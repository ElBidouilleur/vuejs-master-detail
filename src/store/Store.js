/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : Store.js
 * @Created_at : 08/06/2020
 * @Update_at : 08/06/2020
 * --------------------------------------------------------------------------
 */

import Vue from 'vue';
import Vuex from 'vuex';

import DepartmentState from "./Department/DepartmentState";
import DepartmentGetters from "./Department/DepartmentGetters";
import DepartmentActions from "./Department/DepartmentActions";
import DepartmentMutations from "./Department/DepartmentMutations";

Vue.use(Vuex);

export default new Vuex.Store({
    state: DepartmentState,
    getters: DepartmentGetters,
    actions: DepartmentActions,
    mutations: DepartmentMutations,

    modules: {}
});