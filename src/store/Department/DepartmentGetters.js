export default {
    getDepartments: (state) => {
        return state.departments.sort(function (item1, item2) {
            return ((item1.designation === item2.designation) ? 0 : ((item1.designation > item2.designation) ? 1 : -1))
        });
    },
    getDepartment: (state) => (id) => {
        return state.departments.find(department => department.id === id);
    }
}