export default {
    edit({commit}, department) {
        commit('EDIT', department);
    },
    add({commit}, {department, departmentId}) {
        department['id'] = departmentId;
        commit('ADD', department);
    },
    delete({commit}, departmentId) {
        commit('DELETE', departmentId)
    }
}