export default {
    departments: [
        {
            id: 0,
            designation: "Haute-Loire",
            prefecture: "Le Puy-en-Velay",
            population: 2
        },
        {
            id: 1,
            designation: "Puy De Dôme",
            prefecture: "Clermont Ferrand",
            population: 7
        }
    ]
}

