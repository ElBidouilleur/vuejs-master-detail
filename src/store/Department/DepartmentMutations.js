export default {
    EDIT(state, department) {
        let departments = state.departments;
        departments.map((dep) => {
            if (dep.id === department.id) {
                let departmentKey = dep.id
                departments[departmentKey] = department
            }
        })
        state.departments = departments;
    },
    ADD(state, department) {
        let departments = state.departments;
        departments.push(department);
    },
    DELETE(state, departmentId) {
        let departments = state.departments;
        departments.map((dep, key) => {
            if (dep.id === departmentId) {
                departments.splice(key, 1);
            }
        })
    }
}