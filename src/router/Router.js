/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : Router.js
 * @Created_at : 08/06/2020
 * @Update_at : 08/06/2020
 * --------------------------------------------------------------------------
 */

import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from './routes';

export default class Router {
    // ------------------------
    // Constants

    /**
     * Get Vue Router
     *
     * @returns {VueRouter}
     */
    static get router() {
        return new VueRouter({
            mode: 'history',
            base: process.env.BASE_URL,
            routes: routes
        });
    }

    // ------------------------
    // Register

    /**
     * Register the router module in VueJs.
     */
    static register() {
        Vue.use(VueRouter);
    }
}
