/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : routes.js
 * @Created_at : 08/06/2020
 * @Update_at : 08/06/2020
 * --------------------------------------------------------------------------
 */

import Window from "../components/Window";

export default [
    // Page : Accueil
    {
        path: '/',
        name: 'home',
        component: Window,
    },
];
