/**
 * --------------------------------------------------------------------------
 * @Copyright : SAS Spopit
 *
 * @Author : Alexandre Caillot
 *
 * @File : main.js
 * @Created_at : 08/06/2020
 * @Update_at : 08/06/2020
 * --------------------------------------------------------------------------
 */

import Vue from 'vue';

import App from './App';

import Router from './router/Router';
import store from './store/Store';

// --------------------------------
// Configs

Vue.config.productionTip = false;
Vue.config.performance = true;

// --------------------------------
// Register Module

Router.register();

// --------------------------------
// Constant

const router = Router.router;

// --------------------------------
// Entry Point

let rootElement = '#app';
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount(rootElement);
